// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#include "BrickComponent.h"

#include "CasseDiamantsGameMode.h"

UBrickComponent::UBrickComponent() {
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;
}

void UBrickComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (RotationSpeed > 0.f) {
        float rotation = RotationSpeed * DeltaTime;
        FRotator rotator = GetOwner()->GetActorRotation();;
        rotator.Yaw += rotation;
        GetOwner()->SetActorRotation(rotator);
    }
}

// Called when the game starts
void UBrickComponent::BeginPlay() {
    Super::BeginPlay();

    AActor* owner = GetOwner();
    owner->OnActorHit.AddDynamic(this, &UBrickComponent::OnHit);

    ACasseDiamantsGameMode* gameMode = reinterpret_cast<ACasseDiamantsGameMode*>(GetWorld()->GetAuthGameMode());
    if (gameMode)
        gameMode->RegisterBrick(this);
}

void UBrickComponent::OnHit(AActor* SelfActor, AActor* otherActor, FVector NormalInpulse, const FHitResult& Hit) {
    ACasseDiamantsGameMode* gameMode = reinterpret_cast<ACasseDiamantsGameMode*>(GetWorld()->GetAuthGameMode());
    if (gameMode)
        gameMode->NotifyBrickDestruction(this);

    GetOwner()->Destroy();
}

