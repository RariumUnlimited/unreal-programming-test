// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#include "BallComponent.h"

#include "GameFramework/Actor.h"
#include "Math/UnrealMathUtility.h"

#include "CasseDiamantsGameMode.h"

UBallComponent::UBallComponent() {
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UBallComponent::BeginPlay() {
    Super::BeginPlay();

    AActor* owner = GetOwner();
    owner->OnActorHit.AddDynamic(this, &UBallComponent::OnHit);

    pawnMovement = Cast<UPawnMovementComponent>(GetOwner()->GetComponentByClass(UPawnMovementComponent::StaticClass()));

    movementDirection = StartingMovementDirection;
    movementDirection.Z = 0.f;

    ACasseDiamantsGameMode* gameMode = reinterpret_cast<ACasseDiamantsGameMode*>(GetWorld()->GetAuthGameMode());
    if (gameMode)
        gameMode->RegisterBall(this);
}


// Called every frame
void UBallComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (pawnMovement && enableMovement) {
        pawnMovement->AddInputVector(movementDirection * 10);
    }
}

void UBallComponent::OnHit(AActor* SelfActor, AActor* otherActor, FVector NormalInpulse, const FHitResult& Hit) {
    FVector normal = Hit.Normal;
    normal.Z = 0.f;

    movementDirection = FMath::GetReflectionVector(movementDirection, Hit.Normal);
    movementDirection.Z = 0.f;
}
