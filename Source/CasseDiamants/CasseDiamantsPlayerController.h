// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"

#include "CasseDiamantsPlayerController.generated.h"

UCLASS()
class ACasseDiamantsPlayerController : public APlayerController {
    GENERATED_BODY()

 public:
    void BeginPlay() override;

    /// Enable player movement
    inline void EnableMovement() {
        SetInputMode(FInputModeGameOnly());
        bShowMouseCursor = false;
    }

    /// Disable player movement
    inline void DisableMovement() {
        SetInputMode(FInputModeUIOnly());
        bShowMouseCursor = true;
    }

 protected:
    // Begin PlayerController interface
    void PlayerTick(float DeltaTime) override;
    void SetupInputComponent() override;
    // End PlayerController interface

    /**
    *   @brief Called when the player uses the MoveRight axis to move its character
    *   @param value Amount of axis input
    */
    void Move(float value);

    float moveValue = 0.f;  ///< Amount of axis input by the player
    float startingXPosition = 0.f;  ///< Position on the X axis at which the player start, this is used to make sure the player stays on a line

    UPawnMovementComponent* pawnMovement = nullptr;  ///< Reference to the movement component of the player pawn
};


