// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "BallComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CASSEDIAMANTS_API UBallComponent : public UActorComponent {
    GENERATED_BODY()

 public:
	UBallComponent();

    void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    // Enable or disable the actor movement
    inline void SwitchMovement() {
        enableMovement = !enableMovement;
    }

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Game)
    FVector StartingMovementDirection = FVector(-1.f, 0.f, 0.f);  ///< Starting direction of the movement

 protected:
    void BeginPlay() override;

    /**
     * Call when the actor owning the component is hit by something
     * @param selfActor Owning actor
     * @param otherActor Actor that hit selfActor
     * @param normalInpulse ?
     * @param hit Information about the hit between the two actors
     */
    UFUNCTION()
    void OnHit(AActor* SelfActor, AActor* otherActor, FVector NormalInpulse, const FHitResult& Hit);

    UPawnMovementComponent* pawnMovement = nullptr;  ///< Reference to the movement component of the owning actor
    bool enableMovement = false;  ///< True if the ball is allowed to move
    FVector movementDirection;  ///< Direction in which the ball is moving
};
