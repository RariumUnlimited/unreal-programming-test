// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"

#include "GameOverMenu.generated.h"

/**
 * Menu displayed to player when he lose/win the game
 */
UCLASS(Abstract)
class CASSEDIAMANTS_API UGameOverMenu : public UUserWidget {
    GENERATED_BODY()

 public:
    void NativeConstruct() override;

    UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
    class UTextBlock* LastScoreText = nullptr;  ///< Text used to display the player's score for last game

    UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
    class UTextBlock* VictoryText = nullptr;  ///< Text to display when the player wins the game

    UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
    class UTextBlock* GameOverText = nullptr;  ///< Text to display when the player loses the game
};
