// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#include "GameOverMenu.h"

#include "Components/TextBlock.h"

#include "CasseDiamantsGameMode.h"

void UGameOverMenu::NativeConstruct() {
    Super::NativeConstruct();

    ACasseDiamantsGameMode* gameMode = (ACasseDiamantsGameMode*)GetWorld()->GetAuthGameMode();

    if (LastScoreText) {
        int score = gameMode->GetScore();
        LastScoreText->SetText(FText::AsNumber(score));
    }

    if (gameMode->GetVictory())
        GameOverText->SetVisibility(ESlateVisibility::Hidden);
    else
        VictoryText->SetVisibility(ESlateVisibility::Hidden);
}
