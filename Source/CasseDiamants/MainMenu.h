// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"

#include "MainMenu.generated.h"

/**
 * Main menu displayed to the player before starting the game
 */
UCLASS(Abstract)
class CASSEDIAMANTS_API UMainMenu : public UUserWidget {
    GENERATED_BODY()

 public:
    void NativeConstruct() override;

    UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
    class UButton* StartGameButton = nullptr;  ///< Button that the player must click to start the game

    UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
    class UTextBlock* LastScoreText = nullptr;  ///< Text of the score for last game
};
