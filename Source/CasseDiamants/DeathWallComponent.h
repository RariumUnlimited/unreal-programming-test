// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RenderCore.h"

#include "DeathWallComponent.generated.h"

/**
 * Component reprensenting the wall that if the ball hit it, the player loses a life
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CASSEDIAMANTS_API UDeathWallComponent : public UActorComponent {
    GENERATED_BODY()

 public:
    UDeathWallComponent();

    void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Game)
    float DelayBetweenHitRegistration = 0.5f;  ///< Delay before another on the wall is taken into account

 protected:
    void BeginPlay() override;

    /**
     * Call when the actor owning the component is hit by something
     * @param selfActor Owning actor
     * @param otherActor Actor that hit selfActor
     * @param normalInpulse ?
     * @param hit Information about the hit between the two actors
     */
    UFUNCTION()
    void OnHit(AActor* selfActor, AActor* otherActor, FVector normalInpulse, const FHitResult& hit);

    FTimer hitTimer;  ///< Timer for this wall
    float lastHitTime;  ///< We store at which time we were last hit
};
