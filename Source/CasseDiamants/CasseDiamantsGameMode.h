// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#pragma once

#include <set>

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"

#include "BallComponent.h"
#include "BrickComponent.h"

#include "CasseDiamantsGameMode.generated.h"


UCLASS(minimalapi)
class ACasseDiamantsGameMode : public AGameMode {
    GENERATED_BODY()

 public:
    ACasseDiamantsGameMode();

    /**
    *   @brief Change the ui widget displayed on screen
    *   @param NewWidgetClass Class of the new widget to display, nullptr to display nothing
    */
    UFUNCTION(BlueprintCallable, Category = Game)
    void ChangeMenuWidget(TSubclassOf<UUserWidget> newWidgetClass);

    /**
    *   @brief Start the game, reset the level and score, enable player movement
    */
    UFUNCTION(BlueprintCallable, Category = Game)
    void StartGame();
    /**
    *   @brief End the game
    */
    UFUNCTION(BlueprintCallable, Category = Game)
    void EndGame();

    /// Get the score of current/last game
    UFUNCTION(BlueprintCallable, Category = Game)
    inline int GetScore() { return score; }

    /// Get the life of the player
    UFUNCTION(BlueprintCallable, Category = Game)
    inline int GetLife() { return life; }

    /// Return true if the player won the game
    UFUNCTION(BlueprintCallable, Category = Game)
    inline bool GetVictory() { return victory; }

    /// Notify the game mode that the player has lost a life
    UFUNCTION(BlueprintCallable, Category = Game)
    void NotifyPlayerLifeLost();

    /// Register a brick that the player can break to win the game
    void RegisterBrick(UBrickComponent* brick);

    /// Notify that a brick a been destroyed
    void NotifyBrickDestruction(UBrickComponent* brick);

    /// Register a ball to the game mode
    inline void RegisterBall(UBallComponent* ball) {
        gameBall = ball;
    }

    /** The widget class we will use as our main menu when the game starts. */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Game)
    TSubclassOf<UUserWidget> MainMenuWidgetClass;

    /** The widget class we will use as our game over menu when the game ends. */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Game)
    TSubclassOf<UUserWidget> GameOverWidgetClass;

    /** The widget class we will use as our game over menu during the game. */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Game)
    TSubclassOf<UUserWidget> GameWidgetClass;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Game)
    int StartingPlayerLife = 3;  ///< Starting life of the player

 protected:
    /** Called when the game starts. */
    void BeginPlay() override;

    /** The widget instance that we are using as our menu. */
    UPROPERTY()
    UUserWidget* currentWidget = nullptr;

    /** The current ball of the game */
    UPROPERTY()
    UBallComponent* gameBall = nullptr;

    int score = 0;  ///< Score of the player for current/last game
    int life = 3;  ///< Current life of the player
    bool victory = false;  ///< True if the player won the game
    std::set<UBrickComponent*> bricks;  ///< The list of bricks in the game, when there is none remaining the player won the game
};



