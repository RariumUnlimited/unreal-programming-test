// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#include "CasseDiamants.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, CasseDiamants, "CasseDiamants");

DEFINE_LOG_CATEGORY(LogCasseDiamants)
