// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#include "MainMenu.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"

#include "CasseDiamantsGameMode.h"

void UMainMenu::NativeConstruct() {
    Super::NativeConstruct();

    ACasseDiamantsGameMode* gameMode = reinterpret_cast<ACasseDiamantsGameMode*>(GetWorld()->GetAuthGameMode());

    if (LastScoreText) {
        int score = gameMode->GetScore();
        LastScoreText->SetText(FText::AsNumber(score));
    }

    if (StartGameButton)
        StartGameButton->OnClicked.AddDynamic(gameMode, &ACasseDiamantsGameMode::StartGame);
}
