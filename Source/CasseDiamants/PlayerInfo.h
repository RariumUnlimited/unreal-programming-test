// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"

#include "PlayerInfo.generated.h"

/**
 *  User interface used to display player informations during gameplay
 */
UCLASS()
class CASSEDIAMANTS_API UPlayerInfo : public UUserWidget {
    GENERATED_BODY()

 public:
    UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
    class UTextBlock* ScoreText = nullptr;  ///< Text used to display player score

    UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
    class UTextBlock* LifeText = nullptr;  ///< Text used to display player remaining life

    /// Update the score of the player displayed
    void UpdateScore(int score);

    /// Update the life of the player displayed
    void UpdateLife(int life);
};
