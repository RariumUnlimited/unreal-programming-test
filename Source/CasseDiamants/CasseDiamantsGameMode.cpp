// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#include "CasseDiamantsGameMode.h"

#include "UObject/ConstructorHelpers.h"

#include "CasseDiamantsCharacter.h"
#include "CasseDiamantsPlayerController.h"
#include "PlayerInfo.h"

ACasseDiamantsGameMode::ACasseDiamantsGameMode() {
    // use our custom PlayerController class
    PlayerControllerClass = ACasseDiamantsPlayerController::StaticClass();

    // set default pawn class to our Blueprinted character
    static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/CasseDiamants/Blueprints/TopDownCharacter"));
    if (PlayerPawnBPClass.Class != NULL) {
        DefaultPawnClass = PlayerPawnBPClass.Class;
    }
}

void ACasseDiamantsGameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> newWidgetClass) {
    if (currentWidget != nullptr) {
        currentWidget->RemoveFromViewport();
        currentWidget = nullptr;
    }

    if (newWidgetClass != nullptr) {
        currentWidget = CreateWidget<UUserWidget>(GetWorld(), newWidgetClass);
        if (currentWidget != nullptr) {
            currentWidget->AddToViewport();
        }
    }
}

void ACasseDiamantsGameMode::BeginPlay() {
    Super::BeginPlay();
    ChangeMenuWidget(MainMenuWidgetClass);
}

void ACasseDiamantsGameMode::StartGame() {
    ChangeMenuWidget(GameWidgetClass);
    score = 0;
    life = StartingPlayerLife;
    victory = false;

    UPlayerInfo* playerInfo = reinterpret_cast<UPlayerInfo*>(currentWidget);
    if (playerInfo) {
        playerInfo->UpdateScore(score);
        playerInfo->UpdateLife(life);
    }

    ACasseDiamantsPlayerController* pc = reinterpret_cast<ACasseDiamantsPlayerController*>(GetWorld()->GetFirstPlayerController());
    pc->EnableMovement();
    if (gameBall)
        gameBall->SwitchMovement();
}

void ACasseDiamantsGameMode::EndGame() {
    if (bricks.size() == 0)
        victory = true;
    else
        bricks.clear();

    ACasseDiamantsPlayerController* pc = reinterpret_cast<ACasseDiamantsPlayerController*>(GetWorld()->GetFirstPlayerController());
    pc->DisableMovement();

    if (gameBall)
        gameBall->SwitchMovement();

    ChangeMenuWidget(GameOverWidgetClass);
}

void ACasseDiamantsGameMode::RegisterBrick(UBrickComponent* brick) {
    bricks.insert(brick);
}

void ACasseDiamantsGameMode::NotifyBrickDestruction(UBrickComponent* brick) {
    bricks.erase(brick);
    ++score;

    UPlayerInfo* playerInfo = reinterpret_cast<UPlayerInfo*>(currentWidget);
    if (playerInfo) {
        playerInfo->UpdateScore(score);
    }

    if (bricks.size() == 0)
        EndGame();
}

void ACasseDiamantsGameMode::NotifyPlayerLifeLost() {
    --life;

    UPlayerInfo* playerInfo = reinterpret_cast<UPlayerInfo*>(currentWidget);
    if (playerInfo) {
        playerInfo->UpdateLife(life);
    }

    if (life == 0)
        EndGame();
}
