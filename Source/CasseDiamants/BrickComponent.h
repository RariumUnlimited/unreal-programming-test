// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"

#include "BrickComponent.generated.h"

/**
 * A component that must be used by bricks
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CASSEDIAMANTS_API UBrickComponent : public UActorComponent {
    GENERATED_BODY()

 public:
    UBrickComponent();

    void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Game)
    float RotationSpeed = 10.f;

 protected:
    void BeginPlay() override;

    /**
     * Call when the actor owning the component is hit by something
     * @param selfActor Owning actor
     * @param otherActor Actor that hit selfActor
     * @param normalInpulse ?
     * @param hit Information about the hit between the two actors
     */
    UFUNCTION()
    void OnHit(AActor* SelfActor, AActor* otherActor, FVector NormalInpulse, const FHitResult& Hit);
};
