// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#include "CasseDiamantsPlayerController.h"

#include "Engine/World.h"

#include "CasseDiamantsCharacter.h"
#include "CasseDiamantsGameMode.h"

void ACasseDiamantsPlayerController::BeginPlay() {
    Super::BeginPlay();
    DisableMovement();

    AActor* pawn = GetPawn();

    startingXPosition = pawn->GetActorLocation().X;

    pawnMovement = Cast<UPawnMovementComponent>(pawn->GetComponentByClass(UPawnMovementComponent::StaticClass()));
}

void ACasseDiamantsPlayerController::PlayerTick(float DeltaTime) {
    Super::PlayerTick(DeltaTime);

    AActor* pawn = GetPawn();
    FVector pawnLocation = pawn->GetActorLocation();
    if (pawnLocation.X != startingXPosition) {
        pawnLocation.X = startingXPosition;
        pawn->SetActorLocation(pawnLocation);
    }

    if (moveValue != 0.f) {
        pawnMovement->AddInputVector(moveValue * FVector(0.f, 1.f, 0.f));
    }
}

void ACasseDiamantsPlayerController::SetupInputComponent() {
    // set up gameplay key bindings
    Super::SetupInputComponent();

    InputComponent->BindAxis("MoveRight", this, &ACasseDiamantsPlayerController::Move);

    ACasseDiamantsGameMode* gameMode = reinterpret_cast<ACasseDiamantsGameMode*>(GetWorld()->GetAuthGameMode());

    InputComponent->BindAction("ResetLevel", IE_Pressed, gameMode, &ACasseDiamantsGameMode::EndGame);
}

void ACasseDiamantsPlayerController::Move(float value) {
    moveValue = value;
}
