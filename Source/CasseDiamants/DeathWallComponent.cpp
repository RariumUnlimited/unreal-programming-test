// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#include "DeathWallComponent.h"

#include "CasseDiamantsGameMode.h"

UDeathWallComponent::UDeathWallComponent() {
    PrimaryComponentTick.bCanEverTick = true;
}

void UDeathWallComponent::BeginPlay() {
    Super::BeginPlay();

    AActor* owner = GetOwner();
    owner->OnActorHit.AddDynamic(this, &UDeathWallComponent::OnHit);
}

void UDeathWallComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    hitTimer.Tick(DeltaTime);
}

void UDeathWallComponent::OnHit(AActor* SelfActor, AActor* otherActor, FVector NormalInpulse, const FHitResult& Hit) {
    ACasseDiamantsGameMode* gameMode = reinterpret_cast<ACasseDiamantsGameMode*>(GetWorld()->GetAuthGameMode());

    if (gameMode && hitTimer.GetCurrentTime() > lastHitTime + DelayBetweenHitRegistration) {
        gameMode->NotifyPlayerLifeLost();
        lastHitTime = hitTimer.GetCurrentTime();
    }
}

