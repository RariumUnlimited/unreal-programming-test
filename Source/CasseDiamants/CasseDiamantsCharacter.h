// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "CasseDiamantsCharacter.generated.h"

UCLASS(Blueprintable)
class ACasseDiamantsCharacter : public ACharacter {
    GENERATED_BODY()

 public:
    ACasseDiamantsCharacter();

    /** Returns TopDownCameraComponent subobject **/
    FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
    /** Returns CameraBoom subobject **/
    FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

 private:
    /** Top down camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* TopDownCameraComponent;

    /** Camera boom positioning the camera above the character */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* CameraBoom;
};

