// Copyright RariumUnlimited - Please do not use, reproduce or copy this work without permission
#include "PlayerInfo.h"

#include "Components/TextBlock.h"

#include "CasseDiamantsGameMode.h"

void UPlayerInfo::UpdateScore(int score) {
    if (ScoreText) {
        ScoreText->SetText(FText::AsNumber(score));
    }
}

void UPlayerInfo::UpdateLife(int life) {
    if (LifeText) {
        LifeText->SetText(FText::AsNumber(life));
    }
}
