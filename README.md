# Casse Diamants #
## About ##

Casse Diamants (literally "Break Diamonds") is a breakout-style game created for the purpose of the recruitment process for a position at CerebralFix.

The player is reincarnated as a breakout pad and must break some (not-so-)shiny diamonds to win the game.

## Game mechanics ##

The player is able to move left and right and must prevent a ball from touching a wall behind him by running into the ball and making it bounces and hit diamonds to destroy them.

The game is over when the balls hit the wall too much or when all diamonds are destroyed. A score is given to the player depending on how many diamonds are destroyed.

### Controls ###

- Q : Move left
- D : Move right
- P : Prematurely end game

Beware, the ball start moving as soon as you click Start Game on the main menu.

## Building ##

The game can be built using Unreal Engine 4.23.

### Asset reused ###

- Graphic assets from Top Down Unreal Engine 4 template. The TopDown character blueprint (from the template) is used because of a design choice and so parts of its code, however I have included a Pad_Blueprint in the project content that can be used in place of TopDown character blueprint for the game (to prove that I do not depend on the template code for my submission).

### Blueprint usage ###

- Game Over menu for playing an animation
- Quitting the game when click on Quit on the main menu